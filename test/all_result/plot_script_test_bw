set terminal postscript color
set output 'side_channel_leakage.eps'
#set size 1.2,1.0
unset grid
set title "Cache Side-channel Leakage and Refinement (observation via miss count)"
set ylabel "#Counterexamples Neutralized"
set xlabel "Time (seconds)"
#set xrange [1:16]
#set yrange [1:100]
#set logscale y
#set xtics ("" 0, "cnt" 1, "fir" 2, "fdct" 3, "expint" 4, "qurt" 5, "nsichneu" 6, "edn" 7, "ludcmp" 8, "ns" 9, "ndes" 10, "bsort100" 11, "adpcm" 12, "st" 13)
#set xtics ("1" 0, "2" 1, "3" 2, "4" 3, "5" 4, "6" 5, "7" 6, "8" 7, "9" 8, "10" 9, "11" 10, "12" 11, "13" 12, "14" 13, "15" 14, "16" 15)
#set xtics mirror rotate by -45
#unset xtics
set boxwidth 0.9 relative
set style histogram
set style data histogram
set style fill solid 1.0 border -1
set border
set key above
#unset key
set style data linespoints

#plot 'aes.decision.time' using (150+$1):2 lw 2 ps 2 title 'AES from OpenSSL',\
#		 'des.decision.time' using (91+$1):(column(0)) lw 1 ps 2 title 'DES from OpenSSL'

unset title
set output 'des_leakage.eps'
set samples 100
set ylabel "#observation equivalence class"
plot 'des_1way_1KB.time.log' using (100+$1):(column(0)) lw 2 lt -1 pi -4 pt "1" pointinterval 50 title '1-way, 1KB',\
 'des_2way_1KB_LRU.time.log' using (100+$1):(column(0)) lw 2 lt -1 pi -3 pt 7 pointinterval 50 title '2-way, 1KB (LRU)',\
 'des_2way_1KB_FIFO.time.log' using (100+$1):(column(0)) lw 2 lt -1 pi -6 pt 9 pointinterval 50 title '2-way, 1KB (FIFO)',\
 'des_1way_2KB.time.log' using (100+$1):(column(0)) lw 2 lt -1 pi -3 pt "2" pointinterval 50 title '1-way, 2KB',\
 'des_2way_2KB_LRU.time.log' using (100+$1):(column(0)) lw 2 lt -1 pi -5 pt 5 pointinterval 50 title '2-way, 2KB (LRU)',\
 'des_2way_2KB_FIFO.time.log' using (100+$1):(column(0)) lw 2 lt -1 pointinterval 50 title '2-way, 2KB (FIFO)',\
 'des_1way_4KB.time.log' using (100+$1):(column(0)) lw 2 lt -1 pi -4 pt "O" pointinterval 50 title '1-way, 4KB',\
 'des_4way_4KB_LRU.time.log' using (100+$1):(column(0)) lw 2 lt -1 pi -4 pt "#" pointinterval 50 title '4-way, 4KB (LRU)',\
 'des_4way_4KB_FIFO.time.log' using (100+$1):(column(0)) lw 2 lt -1 pi -4 pt "$" pointinterval 50 title '4-way, 4KB (FIFO)',\
 'des_1way_8KB.time.log' using (100+$1):(column(0)) lw 2 lt -1 pi -4 pt "%" pointinterval 50 title '1-way, 8KB',\
 'des_4way_8KB_LRU.time.log' using (100+$1):(column(0)) lw 2 lt -1 pi -4 pt "*"  pointinterval 50 title '4-way, 8KB (LRU)',\
 'des_4way_8KB_FIFO.time.log' using (100+$1):(column(0)) lw 2 lt -1 pi -4 pt "X" pointinterval 50 title '4-way, 8KB (FIFO)'

set output 'aes_leakage.eps'
set ytics 2
set ylabel "#observation equivalence class"
plot 'aes_1way_1KB.time.log' using (149+$1):(column(0)) lw 2 lt -1 pi -4 pt "1" ps 5 pointinterval 1 title '1-way, 1KB',\
 'aes_2way_1KB_LRU.time.log' using (149+$1):(column(0)) lw 2 lt -1 pi -3 pt 7 pointinterval 1 title '2-way, 1KB (LRU)',\
 'aes_2way_1KB_FIFO.time.log' using (149+$1):(column(0)) lw 2 lt -1 pi -6 pt 9 pointinterval 1 title '2-way, 1KB (FIFO)',\
 'aes_1way_2KB.time.log' using (149+$1):(column(0)) lw 2 lt -1 pi -3 pt "2" pointinterval 1 title '1-way, 2KB',\
 'aes_2way_2KB_LRU.time.log' using (149+$1):(column(0)) lw 2 lt -1 pi -5 pt 5 pointinterval 1 title '2-way, 2KB (LRU)',\
 'aes_2way_2KB_FIFO.time.log' using (149+$1):(column(0)) lw 2 lt -1  pointinterval 1 title '2-way, 2KB (FIFO)',\
 'aes_1way_4KB.time.log' using (149+$1):(column(0)) lw 2 lt -1 pi -4 pt "O" pointinterval 1 title '1-way, 4KB',\
 'aes_4way_4KB_LRU.time.log' using (149+$1):(column(0)) lw 2 lt -1 pi -4 pt "#" ps 5 pointinterval 1 title '4-way, 4KB (LRU)',\
 'aes_4way_4KB_FIFO.time.log' using (149+$1):(column(0)) lw 2 lt -1 pi -4 pt "$" ps 5 pointinterval 1 title '4-way, 4KB (FIFO)',\
 'aes_1way_8KB.time.log' using (149+$1):(column(0)) lw 2 lt -1 pi -4 pt "%"  ps 5 pointinterval 1 title '1-way, 8KB',\
 'aes_4way_8KB_LRU.time.log' using (149+$1):(column(0)) lw 2 lt -1 pi -4 pt "*" ps 5 pointinterval 1 title '4-way, 8KB (LRU)',\
 'aes_4way_8KB_FIFO.time.log' using (149+$1):(column(0)) lw 2 lt -1 pi -4 pt "X" ps 5 pointinterval 1 title '4-way, 8KB (FIFO)'


set output 'frac_leakage.eps'
set ylabel "#observation equivalence class"
plot 'frac_1way_1KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "1" title '1-way, 1KB',\
 'frac_2way_1KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -3 pt 7 title '2-way, 1KB (LRU)',\
 'frac_2way_1KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -6 pt 9 title '2-way, 1KB (FIFO)',\
 'frac_1way_2KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -3 pt "2" title '1-way, 2KB',\
 'frac_2way_2KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -5 pt 5 title '2-way, 2KB (LRU)',\
 'frac_2way_2KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 title '2-way, 2KB (FIFO)',\
 'frac_1way_4KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "O" title '1-way, 4KB',\
 'frac_4way_4KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "#"  title '4-way, 4KB (LRU)',\
 'frac_4way_4KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "$"  title '4-way, 4KB (FIFO)',\
 'frac_1way_8KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "%"  title '1-way, 8KB',\
 'frac_4way_8KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "*"  title '4-way, 8KB (LRU)',\
 'frac_4way_8KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "X" title '4-way, 8KB (FIFO)'

unset key

set output 'eccpoint_leakage.eps'
set ylabel "#observation equivalence class"
plot 'eccpoint_validate_1way_1KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "1" title '1-way, 1KB',\
 'eccpoint_validate_2way_1KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -3 pt 7 title '2-way, 1KB (LRU)',\
 'eccpoint_validate_2way_1KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -6 pt 9 title '2-way, 1KB (FIFO)',\
 'eccpoint_validate_1way_2KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -3 pt "2" title '1-way, 2KB',\
 'eccpoint_validate_2way_2KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -5 pt 5 title '2-way, 2KB (LRU)',\
 'eccpoint_validate_2way_2KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 title '2-way, 2KB (FIFO)',\
 'eccpoint_validate_1way_4KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "O" title '1-way, 4KB',\
 'eccpoint_validate_4way_4KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "#"  title '4-way, 4KB (LRU)',\
 'eccpoint_validate_4way_4KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "$"  title '4-way, 4KB (FIFO)',\
 'eccpoint_validate_1way_8KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "%"  title '1-way, 8KB',\
 'eccpoint_validate_4way_8KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "*"  title '4-way, 8KB (LRU)',\
 'eccpoint_validate_4way_8KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "X" title '4-way, 8KB (FIFO)'

set output 'gdkkeyuni_leakage.eps'
set ylabel "#observation equivalence class"
plot 'gdkkeyuni_validate_1way_1KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "1" title '1-way, 1KB',\
 'gdkkeyuni_validate_2way_1KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -3 pt 7 title '2-way, 1KB (LRU)',\
 'gdkkeyuni_validate_2way_1KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -6 pt 9 title '2-way, 1KB (FIFO)',\
 'gdkkeyuni_validate_1way_2KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -3 pt "2" title '1-way, 2KB',\
 'gdkkeyuni_validate_2way_2KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -5 pt 5 title '2-way, 2KB (LRU)',\
 'gdkkeyuni_validate_2way_2KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 title '2-way, 2KB (FIFO)',\
 'gdkkeyuni_validate_1way_4KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "O" title '1-way, 4KB',\
 'gdkkeyuni_validate_4way_4KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "#"  title '4-way, 4KB (LRU)',\
 'gdkkeyuni_validate_4way_4KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "$"  title '4-way, 4KB (FIFO)',\
 'gdkkeyuni_validate_1way_8KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "%"  title '1-way, 8KB',\
 'gdkkeyuni_validate_4way_8KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "*"  title '4-way, 8KB (LRU)',\
 'gdkkeyuni_validate_4way_8KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "X" title '4-way, 8KB (FIFO)'

set output 'gdkkeyname_leakage.eps'
set ylabel "#observation equivalence class"
plot 'gdkkeyname_validate_1way_1KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "1" title '1-way, 1KB',\
 'gdkkeyname_validate_2way_1KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -3 pt 7 title '2-way, 1KB (LRU)',\
 'gdkkeyname_validate_2way_1KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -6 pt 9 title '2-way, 1KB (FIFO)',\
 'gdkkeyname_validate_1way_2KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -3 pt "2" title '1-way, 2KB',\
 'gdkkeyname_validate_2way_2KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -5 pt 5 title '2-way, 2KB (LRU)',\
 'gdkkeyname_validate_2way_2KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 title '2-way, 2KB (FIFO)',\
 'gdkkeyname_validate_1way_4KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "O" title '1-way, 4KB',\
 'gdkkeyname_validate_4way_4KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "#"  title '4-way, 4KB (LRU)',\
 'gdkkeyname_validate_4way_4KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "$"  title '4-way, 4KB (FIFO)',\
 'gdkkeyname_validate_1way_8KB.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "%"  title '1-way, 8KB',\
 'gdkkeyname_validate_4way_8KB_LRU.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "*"  title '4-way, 8KB (LRU)',\
 'gdkkeyname_validate_4way_8KB_FIFO.time.log' using (20+$1):(column(0)) lw 2 lt -1 pi -4 pt "X" title '4-way, 8KB (FIFO)'

unset grid

#set title "Overhead of counterexample exploration and patching"
unset title
set key
set output 'aes_leakage_monitor.eps'
set logscale x
set ylabel "#observation equivalence class"
plot 'aes_1way_1KB.time.log' using (170+$1):(column(0)) lw 2 lt -1 pi -4 pt 6 ps 1 pointinterval 1 title 'timing-based attacks',\
 'aes_1way_1KB.trace.time.log' using (170+$1):(column(0)) lw 2 lt -1 title  'trace-based attacks'

set output 'des_leakage_monitor.eps'
set logscale x
#set style line 1 lt 2 lc rgb "red"
#unset logscale x
set ylabel "#observation equivalence class"
plot 'des_1way_1KB.time.log' using (110+$1):(column(0)) lw 2 lt -1 pi -4 pt 6 ps 1 title 'timing-based attacks',\
 'des_1way_1KB.trace.time.log' using (110+$1):(column(0)) lw 2 lt -1  title 'trace-based attacks'

set output 'frac_leakage_monitor.eps'
set logscale x
set ylabel "#observation equivalence class"
plot 'frac_1way_1KB.time.log' using (23+$1):(column(0)) lw 2 lt -1 pi -4 pt 6 title 'timing-based attacks',\
 'frac_1way_1KB.trace.time.log' using (23+$1):(column(0)) lw 2 lt -1 title 'trace-based attacks'


unset title
unset key

set output 'eccpoint_leakage_monitor.eps'
set logscale x
set ylabel "#observation equivalence class"
plot 'eccpoint_validate_1way_1KB.time.log' using (33+$1):(column(0)) lw 2 lt -1 pi -4 pt 6 title 'timing-based attacks',\
 'eccpoint_validate_1way_1KB.trace.time.log' using (33+$1):(column(0)) lw 2 lt -1 title 'trace-based attacks'

unset key
set output 'gdkkeyuni_leakage_monitor.eps'
set logscale x
set ylabel "#observation equivalence class"
plot 'gdkkeyuni_validate_1way_1KB.time.log' using (30+$1):(column(0)) lw 2 lt -1 pi -4 pt 6 title 'timing-based attacks',\
 'gdkkeyuni_validate_1way_1KB.trace.time.log' using (30+$1):(column(0)) lw 2 lt -1 title 'trace-based attacks'

unset key
set output 'gdkkeyname_leakage_monitor.eps'
set logscale x
set ylabel "#observation equivalence class"
plot 'gdkkeyname_validate_1way_1KB.time.log' using (23+$1):(column(0)) lw 2 lt -1 pi -4 pt 6 title 'timing-based attacks',\
 'gdkkeyname_validate_1way_1KB.trace.time.log' using (23+$1):(column(0)) lw 2 lt -1 title 'trace-based attacks'
