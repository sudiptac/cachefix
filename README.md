# README #

This is a working repository for cache side-channel checker based on CBMC.

### What is this repository for? ###

* Cache side-channel verifier for timing-based and trace-based attack models

### How do I get set up? ###

* Download CBMC
* Replace the directory `cbmc/src` with the directory `cache_checker/src` in the repository
* Follow the compilation instructions for CBMC
* Download Z3
* Install Z3

### What should I know to run the tool? ###

* Currently the cache checker is based _only_ on Z3 backend
* We do not support other solver backends of CBMC: Minisat, CVC4 etc. 
* To run the the cache checker, use the following command: 
`cbmc --trace --slice-formula --smt2 <test_file_path>`
* The cache checker may run forever. Hence, it is a good idea to set a timeout.

### Who do I talk to? ###

* Contact: Sudipta Chattopadhyay (sudiptaonline@gmail.com)