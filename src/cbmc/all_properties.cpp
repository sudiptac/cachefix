/*******************************************************************\

Module: Symbolic Execution of ANSI-C

Author: Daniel Kroening, kroening@kroening.com

\*******************************************************************/

#include <iostream>

#include <util/time_stopping.h>
#include <util/xml.h>
#include <util/json.h>

#include <solvers/sat/satcheck.h>
#include <solvers/prop/literal_expr.h>

#include <goto-symex/build_goto_trace.h>
#include <goto-programs/xml_goto_trace.h>
#include <goto-programs/json_goto_trace.h>

#include "bv_cbmc.h"

#include "all_properties_class.h"

/*******************************************************************\

Function: bmc_all_propertiest::goal_covered

  Inputs:

 Outputs:

 Purpose:

\*******************************************************************/

void bmc_all_propertiest::goal_covered(const cover_goalst::goalt &)
{
#ifdef __DEBUG_CHECKER
	fprintf(stdout, "[CACHE_CHECKER] Goal covered here\n");
#endif

  for(auto &g : goal_map)
  {
    // failed already?
    if(g.second.status==goalt::statust::FAILURE)
      continue;

    // check whether failed
    for(auto &c : g.second.instances)
    {
			//sudiptac: literals are the conditions to check for while generating 
			//all counterexample traces 
      literalt cond=c->cond_literal;

      if(solver.l_get(cond).is_false())
      {
        g.second.status=goalt::statust::FAILURE;
        symex_target_equationt::SSA_stepst::iterator next=c;
        next++; // include the assertion
        build_goto_trace(bmc.equation, next, solver, bmc.ns,
                         g.second.goto_trace);
        break;
      }
    }
  }
}

/*******************************************************************\

Function: bmc_all_propertiest::operator()

  Inputs:

 Outputs:

 Purpose:

\*******************************************************************/

safety_checkert::resultt bmc_all_propertiest::operator()()
{
  status() << "Passing problem to " << solver.decision_procedure_text() << eom;

	// sudiptac: required for the side-channel checker
	exprt side_channel_expr;

  solver.set_message_handler(get_message_handler());

  // stop the time
  absolute_timet sat_start=current_time();

  bmc.do_conversion();

  // Collect _all_ goals in `goal_map'.
  // This maps property IDs to 'goalt'
  forall_goto_functions(f_it, goto_functions)
    forall_goto_program_instructions(i_it, f_it->second.body)
      if(i_it->is_assert())
        goal_map[i_it->source_location.get_property_id()]=goalt(*i_it);

  // get the conditions for these goals from formula
  // collect all 'instances' of the properties
  for(symex_target_equationt::SSA_stepst::iterator
      it=bmc.equation.SSA_steps.begin();
      it!=bmc.equation.SSA_steps.end();
      it++)
  {
    if(it->is_assert() && !it->is_side_channel_goal())
    {
      irep_idt property_id;

      if(it->source.pc->is_assert())
        property_id=it->source.pc->source_location.get_property_id();
      else if(it->source.pc->is_goto())
      {
        // this is likely an unwinding assertion
        property_id=id2string(
          it->source.pc->source_location.get_function())+".unwind."+
          std::to_string(it->source.pc->loop_number);
        goal_map[property_id].description=it->comment;
      }
      else
        continue;

      goal_map[property_id].instances.push_back(it);
    }

		//sudiptac: the following code is required for side-channel checking
		else if(it->is_assert() && it->is_side_channel_goal()) {
#ifdef __DEBUG_CHECKER
			fprintf(stdout, "[CACHE_CHECKER] We hit the side-channel goal, ready to check the presence of side channels.....\n");
#endif
      std::vector<exprt> tmp;
			//side-channel goals have exactly one instance, hence we reserve size one
      tmp.reserve(1);
      tmp.push_back(literal_exprt(it->cond_literal));
      side_channel_expr = conjunction(tmp);
			//safety_checkert::resultt result_side_channel = do_side_channel_check_and_repair(it->cond_expr);
			//return result_side_channel;
		}
  }

  do_before_solving();

#ifdef __CACHE_CHECKER
	safety_checkert::resultt result_side_channel = do_side_channel_check_and_repair(side_channel_expr);
	return result_side_channel;
#endif

  cover_goalst cover_goals(solver);

  cover_goals.set_message_handler(get_message_handler());
  cover_goals.register_observer(*this);

  for(const auto &g : goal_map)
  {
    // Our goal is to falsify a property, i.e., we will
    // add the negation of the property as goal.
    literalt p=!solver.convert(g.second.as_expr());
    cover_goals.add(p);
  }

  status() << "Running " << solver.decision_procedure_text() << eom;

  bool error=false;

  decision_proceduret::resultt result=cover_goals();

  if(result==decision_proceduret::resultt::D_ERROR)
  {
    error=true;
    for(auto &g : goal_map)
      if(g.second.status==goalt::statust::UNKNOWN)
        g.second.status=goalt::statust::ERROR;
  }
  else
  {
    for(auto &g : goal_map)
      if(g.second.status==goalt::statust::UNKNOWN)
        g.second.status=goalt::statust::SUCCESS;
  }

  // output runtime

  {
    absolute_timet sat_stop=current_time();
    status() << "Runtime decision procedure: "
             << (sat_stop-sat_start) << "s" << eom;
  }

  // report
  report(cover_goals);

  if(error)
    return safety_checkert::resultt::ERROR;

  bool safe=(cover_goals.number_covered()==0);

  if(safe)
    bmc.report_success(); // legacy, might go away
  else
    bmc.report_failure(); // legacy, might go away

  return safe?safety_checkert::resultt::SAFE:safety_checkert::resultt::UNSAFE;
}

/*******************************************************************\

Function: bmc_all_propertiest::report()

  Inputs:

 Outputs:

 Purpose:

\*******************************************************************/

void bmc_all_propertiest::report(const cover_goalst &cover_goals)
{
  switch(bmc.ui)
  {
  case ui_message_handlert::uit::PLAIN:
    {
      status() << "\n** Results:" << eom;

      for(const auto &goal_pair : goal_map)
        status() << "[" << goal_pair.first << "] "
                 << goal_pair.second.description << ": "
                 << goal_pair.second.status_string()
                 << eom;

      if(bmc.options.get_bool_option("trace"))
      {
        for(const auto &g : goal_map)
          if(g.second.status==goalt::statust::FAILURE)
          {
            std::cout << "\n" << "Trace for " << g.first << ":" << "\n";
            show_goto_trace(std::cout, bmc.ns, g.second.goto_trace);
          }
      }

      status() << "\n** " << cover_goals.number_covered()
               << " of " << cover_goals.size() << " failed ("
               << cover_goals.iterations() << " iteration"
               << (cover_goals.iterations()==1?"":"s")
               << ")" << eom;
    }
    break;

  case ui_message_handlert::uit::XML_UI:
    {
      for(const auto &g : goal_map)
      {
        xmlt xml_result("result");
        xml_result.set_attribute("property", id2string(g.first));
        xml_result.set_attribute("status", g.second.status_string());

        if(g.second.status==goalt::statust::FAILURE)
          convert(bmc.ns, g.second.goto_trace, xml_result.new_element());

        std::cout << xml_result << "\n";
      }
      break;
    }

    case ui_message_handlert::uit::JSON_UI:
    {
      json_objectt json_result;
      json_arrayt &result_array=json_result["result"].make_array();

      for(const auto &g : goal_map)
      {
        json_objectt &result=result_array.push_back().make_object();
        result["property"]=json_stringt(id2string(g.first));
        result["description"]=json_stringt(id2string(g.second.description));
        result["status"]=json_stringt(g.second.status_string());

        if(g.second.status==goalt::statust::FAILURE)
        {
          jsont &json_trace=result["trace"];
          convert(bmc.ns, g.second.goto_trace, json_trace);
        }
      }

      std::cout << ",\n" << json_result;
    }
    break;
  }
}

/*******************************************************************\

Function: bmct::all_properties

  Inputs:

 Outputs:

 Purpose:

\*******************************************************************/

safety_checkert::resultt bmct::all_properties(
  const goto_functionst &goto_functions,
  prop_convt &solver)
{
  bmc_all_propertiest bmc_all_properties(goto_functions, solver, *this);
  bmc_all_properties.set_message_handler(get_message_handler());
  return bmc_all_properties();
}

/*******************************************************************\

Function: bmc_all_propertiest::do_side_channel_check_and_repair()

  Inputs:

 Outputs:

 Purpose: simultaneously check side channels and repair them

\*******************************************************************/

safety_checkert::resultt bmc_all_propertiest::do_side_channel_check_and_repair(exprt& side_channel_expr)
{
	decision_proceduret::resultt result;
  
	cover_goalst cover_goals(solver);

  cover_goals.set_message_handler(get_message_handler());
  cover_goals.register_observer(*this);

#ifdef __NDEBUG_CHECKER
	fprintf(stdout, "[CACHE_CHECKER] Now going to check all properties\n");
#endif

  //for(const auto &g : goal_map)
  //{
    // Our goal is to falsify a property, i.e., we will
    // add the negation of the property as goal.
    //literalt p=!solver.convert(g.second.as_expr());
    literalt p=solver.convert(side_channel_expr);
    cover_goals.add(p);

		do {
  			// stop the time
  			absolute_timet sat_start=current_time();

				status() << "Running " << solver.decision_procedure_text() << eom;

				bool error=false;

				// sudiptac: entry level call to check side-channel goal
				result=cover_goals.side_channel_goal();

  			if(result==decision_proceduret::resultt::D_ERROR)
  			{
    			error=true;
    			for(auto &g : goal_map)
      			if(g.second.status==goalt::statust::UNKNOWN)
        			g.second.status=goalt::statust::ERROR;
  			}
  			else
  			{
    			for(auto &g : goal_map)
      			if(g.second.status==goalt::statust::UNKNOWN)
        			g.second.status=goalt::statust::SUCCESS;
  			}

  			// output runtime

  			{
    			absolute_timet sat_stop=current_time();
    			status() << "Runtime decision procedure: "
             			<< (sat_stop-sat_start) << "s" << eom;
  			}

  			// report
  			report(cover_goals);
				
				if(error)
    			return safety_checkert::resultt::ERROR;
				
				// explore the goal negated with the counterexample(s)
				// sudiptac: TODO
				cover_goals.remove(); //remove the last goal covered

#ifdef __DEBUG_CHECKER
				fprintf(stdout, "[CACHE_CHECKER] Finished analyzing a counterexample.....\n\n");
				fprintf(stdout, "***%0*d***\n\n", 120, 0);
#endif
				// there should be at least one goal, please	
		} while( cover_goals.size() > 0 && result==decision_proceduret::resultt::D_SATISFIABLE );
  //}
  
	// we are done here checking and patching without any error, hence, return that the 
	// program is now safe from any side-channel(s)
	return safety_checkert::resultt::SAFE;
}




