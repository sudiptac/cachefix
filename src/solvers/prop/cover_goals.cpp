/*******************************************************************\

Module: Cover a set of goals incrementally

Author: Daniel Kroening, kroening@kroening.com

\*******************************************************************/

#include <util/threeval.h>

#include "literal_expr.h"
#include "cover_goals.h"

/*******************************************************************\

Function: cover_goalst::~cover_goalst

  Inputs:

 Outputs:

 Purpose:

\*******************************************************************/

cover_goalst::~cover_goalst()
{
}

/*******************************************************************\

Function: cover_goalst::mark

  Inputs:

 Outputs:

 Purpose: Mark goals that are covered

\*******************************************************************/

void cover_goalst::mark()
{
  // notify observers
  for(const auto &o : observers)
    o->satisfying_assignment();

  for(auto &g : goals)
    if(g.status==goalt::statust::UNKNOWN &&
       prop_conv.l_get(g.condition).is_true())
    {
      g.status=goalt::statust::COVERED;
      _number_covered++;

      // notify observers
      for(const auto &o : observers)
        o->goal_covered(g);
    }
}

/*******************************************************************\

Function: cover_goalst::constaint

  Inputs:

 Outputs:

 Purpose: Build clause

\*******************************************************************/

void cover_goalst::constraint()
{
  exprt::operandst disjuncts;

  // cover at least one unknown goal

  for(std::list<goalt>::const_iterator
      g_it=goals.begin();
      g_it!=goals.end();
      g_it++)
    	if(g_it->status==goalt::statust::UNKNOWN &&
       !g_it->condition.is_false())
      disjuncts.push_back(literal_exprt(g_it->condition));

  // this is 'false' if there are no disjuncts
  prop_conv.set_to_true(disjunction(disjuncts));
}

/*******************************************************************\

Function: cover_goalst::freeze_goal_variables

  Inputs:

 Outputs:

 Purpose: Build clause

\*******************************************************************/

void cover_goalst::freeze_goal_variables()
{
  for(std::list<goalt>::const_iterator
      g_it=goals.begin();
      g_it!=goals.end();
      g_it++)
    if(!g_it->condition.is_constant())
      prop_conv.set_frozen(g_it->condition);
}

/*******************************************************************\

Function: cover_goalst::operator()

  Inputs:

 Outputs:

 Purpose: Try to cover all goals

\*******************************************************************/

decision_proceduret::resultt cover_goalst::operator()()
{
  _iterations=_number_covered=0;

  decision_proceduret::resultt dec_result;

  // We use incremental solving, so need to freeze some variables
  // to prevent them from being eliminated.
  freeze_goal_variables();

  do
  {
    // We want (at least) one of the remaining goals, please!
    _iterations++;

    constraint();
    dec_result=prop_conv.dec_solve();

    switch(dec_result)
    {
    case decision_proceduret::resultt::D_UNSATISFIABLE: // DONE
      return dec_result;

    case decision_proceduret::resultt::D_SATISFIABLE:
      // mark the goals we got, and notify observers
      mark();
      break;

    default:
      error() << "decision procedure has failed" << eom;
      return dec_result;
    }
  } while(dec_result==decision_proceduret::resultt::D_SATISFIABLE &&
        number_covered()<size());

  return decision_proceduret::resultt::D_SATISFIABLE;
}

/*******************************************************************\

Function: cover_goalst::mark

  Inputs:

 Outputs:

 Purpose: Mark a specific goal (specifically, side-channel goal)

\*******************************************************************/

void cover_goalst::mark(std::list<goalt>::const_iterator& channel_goal)
{
  // notify observers
  for(const auto &o : observers)
    o->satisfying_assignment();

  _number_covered++;

  // notify observers
  for(const auto &o : observers)
    o->goal_covered(*channel_goal);
}

/*******************************************************************\

Function: cover_goalst::constaint (for side channels)

  Inputs:

 Outputs:

 Purpose: Build clause for side channel checking

\*******************************************************************/

void cover_goalst::constraint(std::list<goalt>::const_iterator& channel_goal)
{
  exprt::operandst disjuncts;

  // cover the side-channel goal and only side-channel goal
	disjuncts.push_back(literal_exprt(channel_goal->condition));

  // this is 'false' if there are no disjuncts
  prop_conv.set_to_true(disjunction(disjuncts));
}

/*******************************************************************\

Function: cover_goalst::side_channel_goal

  Inputs:

 Outputs:

 Purpose: Try to check the satisfiability of a side-channel goal

\*******************************************************************/

decision_proceduret::resultt cover_goalst::side_channel_goal()
{

  decision_proceduret::resultt dec_result;

	// sudiptac: for side-channel checking, goals list must be a singleton
	assert (goals.size() == 1 && "[CACHE_CHECKER] Error: side-channel goal size must be one");
  
	_iterations=_number_covered=0;
  
	std::list<goalt>::const_iterator g_it=goals.begin();

  // We use incremental solving, so need to freeze some variables
  // to prevent them from being eliminated.
  freeze_goal_variables();

#ifdef __DEBUG_CHECKER
	fprintf(stdout, "[CACHE_CHECKER] within the coverage goal\n");
#endif

  constraint(g_it);
  dec_result=prop_conv.dec_solve();

  switch(dec_result)
  {
    case decision_proceduret::resultt::D_UNSATISFIABLE: // DONE
      return dec_result;

    case decision_proceduret::resultt::D_SATISFIABLE:
			// sudiptac: [CACHE_CHECKER] side-channel goals need not be marked as each 
			// time a new goal is prepared and sent to the solver to check satisfiability.
			// The following marking is to maintain legacy.
      mark(g_it);
			break;

    default:
      error() << "decision procedure has failed" << eom;
      return dec_result;
  }

	//for side-channel checking, it is always one iteration
  _iterations++;

#ifdef __DEBUG_CHECKER
	fprintf(stdout, "[CACHE_CHECKER] Returning satisfiability from side-channel checker\n");
#endif

  return decision_proceduret::resultt::D_SATISFIABLE;
}
